class Counter {
  constructor(number) {
    this.number = number,
    this.numberOfCalls = 0
  }

  increment() {
    ++this.numberOfCalls;
    ++this.number;
  }
}

let counter = new Counter(7);
for (let i = 0; i<10; ++i) {
  counter.increment();
  console.log(counter);
}
