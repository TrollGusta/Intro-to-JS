"use strict"

function print(object) {
    console.log(object, " - type is ", typeof (object));
}

let str = "some string";
print(str);

let num = 5.5;
print(num);

let nullVar = null;
print(nullVar);

let undefVar = undefined;
print(undefVar);

let flag = true;
print(flag);

let array = [];
print(array);

for (let idx = 0; idx < 50; ++idx) {
    array[idx] = 2 * idx;
    if (array[idx] % 3 == 0) {
        console.log(array[idx]);
    }
}
