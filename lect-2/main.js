// часть 1
let text = "abaabacbabad";
let pattern = "acba";

console.log(text.includes(pattern));

// часть 2
function formatDate(date) {
  let months = [" Январь ", " Февраль ", " Март ", " Апрель ", " Май ",
    " Июнь ", " Июль ", " Август ", " Сентябрь ", " Октябрь ", " Ноябрь ",
    " Декбрь "];

  return date.getFullYear() + months[date.getMonth()] + date.getDate();
}

let date = new Date();

console.log(formatDate(date));

// часть 3
let array = new Array(1000);
for (let idx = 0; idx < array.length; ++idx) {
  array[idx] = idx;
}

console.log(array.reduce((sum, curr) => {
  if (curr % 2 == 0) return sum + curr;
  else return sum;
}, 0));

// проверка
// console.log(998 / 2 * 500);
